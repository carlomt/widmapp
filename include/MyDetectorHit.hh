//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
//

#ifndef MyDetectorHit_h
#define MyDetectorHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "tls.hh" // FOR MT

class MyDetectorHit : public G4VHit
{
public:

  MyDetectorHit();
  ~MyDetectorHit();
  MyDetectorHit(const MyDetectorHit&);
  const MyDetectorHit& operator=(const MyDetectorHit&);
  G4int operator==(const MyDetectorHit&) const;

  inline void* operator new(size_t);
  inline void  operator delete(void*);

  void Draw();
  void Print();

public:  
  void SetDetectorID  (G4String detectorName) { detectorID = detectorName;};
  void SetEdep (G4double de) { edep = de; };
      
  G4String GetBodyPartID() { return detectorID; };
  G4double GetEdep()    { return edep; };      
      
private:
  G4String detectorID;
  G4double      edep;    
};

typedef G4THitsCollection<MyDetectorHit> MyDetectorHitsCollection;

extern G4ThreadLocal G4Allocator<MyDetectorHit>* MyDetectorHitAllocator;

inline void* MyDetectorHit::operator new(size_t)
{
  if(!MyDetectorHitAllocator)
      MyDetectorHitAllocator = new G4Allocator<MyDetectorHit>;
  return (void *) MyDetectorHitAllocator->MallocSingle();
}

inline void MyDetectorHit::operator delete(void *aHit)
{
  MyDetectorHitAllocator -> FreeSingle((MyDetectorHit*) aHit);
}

#endif
