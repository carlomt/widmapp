#ifndef MyDetectorBuilder_h
#define MyDetectorBuilder_h 1

#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"

#include "globals.hh"

class MyDetectorBuilder
{
public:
  MyDetectorBuilder();
  ~MyDetectorBuilder();

  G4VPhysicalVolume* BuildDetector(const G4ThreeVector& position, G4RotationMatrix& rot);
  void SetMotherVolume(G4VPhysicalVolume*);
  G4VPhysicalVolume* GetVolume();

protected:
  G4VPhysicalVolume* motherVolume;
  G4int nDet;
  G4Material* fMaterial;
};

#endif
