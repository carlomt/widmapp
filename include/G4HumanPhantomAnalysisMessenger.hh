#ifndef G4HumanPhantomAnalysisMessenger_HH
#define G4HumanPhantomAnalysisMessenger_HH

#include "G4UImessenger.hh"

class G4HumanPhantomAnalysisManager;
class G4HumanPhantomAnalysisHelper;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithABool;
class G4UIcmdWithAnInteger;

class G4HumanPhantomAnalysisMessenger : public G4UImessenger {

public:
  G4HumanPhantomAnalysisMessenger(G4HumanPhantomAnalysisManager*);
  virtual ~G4HumanPhantomAnalysisMessenger() override;
  virtual void SetNewValue(G4UIcommand*, G4String) override;

  // G4String GetOutputFileName();
  
private:
  G4HumanPhantomAnalysisManager* fAnalysisManager;
  G4UIdirectory* fAnalysisManagerDir;
  G4UIcmdWithAString* fSetFileName;

  G4HumanPhantomAnalysisHelper* fHelper;
};

#endif
