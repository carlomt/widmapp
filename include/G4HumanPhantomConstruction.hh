//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// Previous authors: G. Guerrieri, S. Guatelli and M. G. Pia, INFN Genova, Italy
// Authors (since 2007): S. Guatelli,University of Wollongong, Australia
// Contributions by F. Ambroglini INFN Perugia, Italy
//
#ifndef G4HumanPhantomConstruction_H
#define G4HumanPhantomConstruction_H 1

#include "G4VUserDetectorConstruction.hh"
#include "G4HumanPhantomMessenger.hh"
#include "G4SystemOfUnits.hh"

#include "globals.hh"
#include <map>
#include <vector>

class G4VPhysicalVolume;
class G4HumanPhantomMaterial;

class G4HumanPhantomConstruction : public G4VUserDetectorConstruction
{
  public:
     G4HumanPhantomConstruction();
    ~G4HumanPhantomConstruction();
     virtual G4VPhysicalVolume* Construct() override;

  void SetBodyPartSensitivity(G4String, G4bool);

  void SetPhantomSex(G4String);
  void SetPhantomModel(G4String);
  virtual void ConstructSDandField() override;
  //G4VPhysicalVolume* GetMotherVolume(){return mother;};

  inline std::vector<G4VPhysicalVolume*> GetDetectorVolumes(){return fDetectors;};
	
	G4double ellipseDerivative(G4double xval) {
		return - (trunkDy+safeDistance)/pow(trunkDx+safeDistance,2)*xval/sqrt(1-pow((xval/(trunkDx+safeDistance)),2));
	};
	void SetMaxDetPosErr(G4double val) {
		posMaxSmearing = val;
	};
 private:
  G4HumanPhantomMaterial* material;
  G4HumanPhantomMessenger* messenger;
  G4VPhysicalVolume* ConstructWorld();

  G4String                 model;
  G4String                 sex;
  std::map<std::string,G4bool> sensitivities;

  std::vector<G4VPhysicalVolume*> fDetectors;
	
	// Retrieve dimensions of Trunk ellipse to place detectors, for now by hand.. [TODO]
	G4double trunkDx = 20. * cm;
  G4double trunkDy = 10. * cm;

	double posMaxSmearing = 0*cm;
	double safeDistance = 0.4*cm;
	std::vector<G4ThreeVector> fDetectorsOrigPos; //vector storing original positions of the detectors. Filled in G4HumanPhantomConstruction constructor
  std::vector<G4double> fDetectorsPosSmearingR; //vector to store radial error in positioning for each detector independently
  std::vector<G4double> fDetectorsPosSmearingTheta; //vector to store the angle generated to obtain the direction of the radial error in positioning for each detector independently
	
	//Post manual smearing detector position
	G4double postSmearX=0;
	G4double postSmearY=0;
	G4double postSmearZ=0;
	G4double postSmearPhi=0;
	
}


;




#endif

