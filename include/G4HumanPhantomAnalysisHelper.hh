#ifndef G4HumanPhantomAnalysisHelper_HH
#define G4HumanPhantomAnalysisHelper_HH

#include "globals.hh"


class G4HumanPhantomAnalysisHelper {
public:
  static G4HumanPhantomAnalysisHelper* GetInstance();
  void SetFileName(const G4String f);
  G4String GetFileName();

private:
  G4HumanPhantomAnalysisHelper();
  
  G4String fName;
  static G4HumanPhantomAnalysisHelper* fInstance;
  
};


#endif
