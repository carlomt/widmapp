%% !TEX TS-program = pdflatexmk

\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc} 
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{color}
\usepackage{setspace} 
\usepackage{comment}
\usepackage{lineno}
\usepackage{subfig}
\usepackage[thickspace,amssymb]{SIunits}
\usepackage{authblk}
\usepackage{hyperref}
\def\code#1{\texttt{#1}}
\newcommand{\cred}{\color{red}}

\title{Note on the WIDMApp feasibility Monte Carlo Simulation.}

\begin{document}

\section{Geometry}
This simulation geometry is composed  by two elements: the patient and the detector system.

\textbf{Patient. }To simulate the patient body we used  the MIRD (Medical International Radiation Dose) %stylised 
phantom~\cite{mird}. This phantom represents an adult man weighing 70 Kg as the combination of simple geometrical figures corresponding approximately to the shape of a human body. The phantom includes internal organs which are defined again by means of simple shapes as subregions of the phantom. Each ``organ'' is considered homogeneous in composition and density among three types of tissue: lung, skeletal and generic soft tissue. The body is represented  with the arms parallel to the torso and the legs stretched, mimicking the tipycal positioning of a patient undergoing CT scans. More details about the Geant4 implementation of the MIRD anthropomorphic phantom can be found in ~\cite{Guatelli}. 
An image of the phantom is shown in \autoref{fig:phantom}. 

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=\textwidth]{phantom2}
\caption{MIRD male anthropomorphic phantom used for the MC simulation. The cyan cylinders nearby the phantom are the detectors.}
\label{fig:phantom}
\end{center}
\end{figure}

The advantage of dealing with a simplified geometrical description of the patient body, with respect for example to a real CT (Computed Tomography), resides in the much reduced computation time needed for the simulation, that comes obviously at the cost of a less accurate reproduction of the problem. However, we identified this compromise as a reasonable starting point, that allows to develop the formalism and technical instruments needed  for the feasibility study.

\textbf{Detectors. }In order to make the simulation code modular and easy to extend, we followed the ``factory method pattern'',  developing a class that creates the geometry and places the detectors. In this way we can for example easily change the number or shape of detectors in our geometry. 
In particular, we started by placing on the phantom 7 detectors, schematised as cylinders with a 13~mm radius and  3~mm height. The number of 7 was chosen to place one detector on each of the organs we considered as active (see next section), with the exception of the thyroid itself, on which we placed 2 detectors, to cover its two lobes. Since at this stage of the simulation the quantity of interest is the flux intercepted by each detector, and not the actual energy deposition in it, we implemented the detectors as hollow, pure geometrical cylinders.
The main geometry class keeps an array of pointers to all the detectors, which are then passed to the Run Action class to save the information of all the particles entering each detector. 

Since the stability and reproducibility of detector positioning is one of the key aspects in the feasibility study of the proposed technique, we also included the possibility to simulate a mispositioning of the detectors. This is achieved by independently sampling, in each run, the radial displacement of each detector, with respect to its ``ideal'' position, i.e. the one in which the detector is faced to the closest possible point to one of the emitting organs. Such displacement is sampled form a uniform distribution, whose width is the same for all detectors, and can be set via a User Interface (UI) command at run time. The two detectors placed on the thyroid were excluded from this  position randomisation, assuming an easier placement for them. 
%For the simulation presented here we used TODO~cm as the position distributions width.

\section{Primaries}
To achieve the maximum flexibility, we used the Geant4 \textit{General Particle Source} to generate the primaries. Therefore, the primary particles properties can be defined at run time using UI commands.
Dealing with $^{131}$I as emitting isotope, we directly generated the 364~keV photons that are produced in the $^{131}$Xe$^{\star}$ de-excitation, which is in turn the product of the $\beta^{-}$ decay of $^{131}$I. We decided to neglect the electrons that are as well produced in the $^{131}$I $\beta^{-}$ decay, since its end point is 606~keV. Having thus a range of the order of 2~mm in tissue and negligible bremsstrahlung emission, such electrons do not have the possibility of generating a signal in the detectors.

For this first feasibility study we decided to consider only the 6 most important active organs as source of particles, namely: the thyroid, the two kidneys, the bladder, the liver, and the spleen. Each of them is considered to be uniformly active, i.e. the position where the primary particle starts is sampled uniformly within the organ volume, its direction being obviously isotropic. 



\section{Physics models}

Since all the physics processes involved in this simulation are electromagnetic, to ensure high precision, reliable and well tested models, we used the standard electromagnetic physics constructor \textit{emstandard\_opt4} (the physics list class name is: \code{G4EmStandardPhysics\_option4}), which is the one with the most accurate standard and low-energy models among the standard electromagnetic physics lists available in the Geant4 package~\cite{geant4-pm}. These models are regularly benchmarked also for medical applications, the most recent being~\cite{geant4-medicalbench}. In addition to the use of the \textit{emstandard\_opt4} physics list, we also lowered the production threshold of secondary particles  to 1~mm as some secondaries can be produced nearby the detectors and contribute to the signal production. 

%\subsection*{Simulation Results Analysis} %Runs and merging
%CM: non mi pare una analisi tornerei al precedente titolo del paragrafo
\section{Runs and merging}
\label{sec:mcout}
We ran six different simulations, one per each considered organ. For each simulation we computed the probability to produce a signal in each detector, i.e. the probability that a particle, either primary or secondary, enters in each detector. 
%``activating'' separately one of the considered organ at the time.
%In each simulation the particles were generated and tracked, taking into account energy deposition and creation of secondary particles. 
%The signal on each detector has thus been obtained as the number of particles traversing each active element.
%In such a way, we computed the probability for a given primary particle in the active $i-$organ to give eventually a signal in the given $j-$detector, populating a 6x7 matrix 
In such a way, we computed the probability for a given primary particle in the active $j-$organ to produce a signal in the $i-$detector, obtaining a $6 \times 7$ matrix $ \varepsilon_{i j}$.
\autoref{fig:mcout} shows this probability matrix.

\begin{table}[htbp]
\begin{center}
\includegraphics[width=\textwidth]{matrix_MCout}
\caption{Probabilities of each organ to produce a signal in each detector, it is referred in the text and equations as a matrix, $\varepsilon_{i j}$.}
\label{fig:mcout}
\end{center}
\end{table}

%Once obtained this probability matrix, we used it, together with some  assumptions on $^{131}$I biokinetic, to foresee the response of each detector as a function of time ($S^{sim}_{i}(t)$).
%These assumptions regard $A_{0 j}$, i.e. the j-th organ activity when the accumulation has ended, and $\tau_{j}$, i.e. the decay time of this activity in a single exponential model.

We then computed offline the response of each detector as a function of time ($S^{sim}_{i}(t)$) with some  assumptions on $^{131}$I biokinetics, namely a single exponential for the activity of each organ and its parameters: the initial activity of $A_{0 j}$ and the time constant $\tau_{j}$.
$S^{sim}_{i}(t)$ is then:
\begin{equation}
S^{sim}_{i}(t) = \sum_{j} \varepsilon_{i j} \, A_{0 j} \exp \left(-t / \tau_{j}\right).
\label{eq:mcsignal}
\end{equation}
%where $S_{i}(t)$ is the signal measured by the  $i$th detector at the time $t$, $A_{0 j}$ and $\tau_{j}$ are the activity at the monitoring start and the effective mean life time for each organ ($j$), being the latter the convolution of the mean life time of the radioisotope and the metabolic washout.

%$\varepsilon_{ij}$ is the probability that a particle emitted in the voxel $j$ reaches the scintillator and is detected by the detector $i$. 
%In the $\varepsilon_{ij}$ calculation the following physical phenomena are included: the radionuclide emissivity, the particle propagation in the patient tissue - taking into account the anatomic characteristics - the attenuation and scatter correction, and the detector. 

To set realistic values for $A_{0 j}$ and $\tau_{j}$  in  \autoref{eq:mcsignal}, we assumed an administration of 2~GBq, being the first thyroid $^{131}$I treatment usually is performed administering between 15 and 60~MBq/Kg. 
{\cred REFERENZA} % % % %
We roughly assigned 30\% of the activity to the thyroid, given its high specificity for Iodine, and 70\% to the rest of the body. We further divided this second part among the 5 other active organs (2 kidneys, liver, spleen and bladder), assuming for them the same specific activity of radiopharmaceutical. Since in this first study we are as discussed considering  the emission only from these organs,  we assumed the skeleton and the skin instead to have a negligible uptake. %{\cred (In realta abbiamo gia detto che consideriamo attivi solo quegli organi...)}.
We finally added a smearing on the simulated detector responses assuming for them a Poissonian distribution.

By means of the described procedure, we simulated to perform measurements by sampling every 10 minutes for the first 7 days following the treatment. \autoref{fig:mcsig}  shows the output of the full simulation procedure.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=.6\textwidth]{mcsig}
\caption{Time Counts Curve output of the Monte Carlo simulation. The blue dots are the simulated sampling of one detector, namely the one faced to the right kidney. The coloured curves {\cred da rifare il plot} shows the contribution of each organ to the signal on this detector. The bottom panel shows the residuals between each point and the exponential function result of the \autoref{eq:mcsignal}, it highlights the  fluctuations added to simulate the Poissonian smearing. }
\label{fig:mcsig}
\end{center}
\end{figure}


\end{document}
