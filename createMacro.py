from __future__ import print_function, division
import subprocess
import re
import argparse
import sys
import math
import os

def count_leading_spaces(string, verbose=False):
	if verbose:
		print("count_leading_spaces",string)
	return len(string) - len(string.lstrip(" "))

def get_name(string, verbose=False):
	if verbose:
		print("get_name",string)
	res = string.split("/")[0].split(":")
	return res[0].replace('"', ''), res[1]

parser = argparse.ArgumentParser()
parser.add_argument('organ', metavar='organ', type=str, 
					help='organ name')
parser.add_argument("-o", "--outfile", help="define output file name")
parser.add_argument("-n", "--nPrim", help="define number of primaries")
parser.add_argument("-R", "--run", help="submit create job on the fly", dest='run', action='store_true')
parser.add_argument("-g", "--loadGeom", help="load also geometry", dest='loadGeom', action='store_true')
parser.add_argument("-e", "--energyPrim", help="define primary photon energy", default=364)
parser.add_argument("-nM", "--numMacros", help="define number of identical macros for smearing studies", default=1)
parser.add_argument("-s", "--smearing", help="define max radial detector mispositioning (flat distribution)", default=2)
parser.add_argument("-v", "--verbose", help="increase output verbosity",
					action="store_true")
   
args = parser.parse_args()

if args.verbose:
        print("Verbose output")

loadGeomFlag=False
rootfile = args.organ

f = open("tmp.mac", "w")
f.write("/control/execute adultMIRDMale.mac \n")
f.write("/vis/drawTree \n")
f.close()


print("running Geant4 to find volume",args.organ)
MyOut = subprocess.Popen(['./phantom', 'tmp.mac'], 
						 stdout=subprocess.PIPE, 
						 stderr=subprocess.STDOUT)
stdout,stderr = MyOut.communicate()

decodedout = stdout.decode("utf-8")

trigger = False

names = []
replicas = []
storePhysVolName = ""

decodedouts = decodedout.split('\n')

for triggerline in range(0,len(decodedouts)):
	line = decodedouts[triggerline]
	if "Now printing with verbosity" in line:
		trigger = True
		
	if trigger and args.organ.lower() in line.lower():
		print("found logical volume at line:",triggerline,"name:",line)
		targetLevel = count_leading_spaces(line)
		print("Volume target level:",targetLevel)
		name, replica = get_name(line)
		names.append(name)
		storePhysVolName=name
		replicas.append(replica)
		print("name:",name,"replica:",replica)
		DLneeded=-2
		print("hierarchy:")
		for ilineback in range(0,triggerline):
			iline = triggerline - ilineback
			line = decodedouts[iline]
			level = count_leading_spaces(line)
			if level - targetLevel== DLneeded:
				if line.startswith("#"):
					break
				print(iline, line)
				name, replica = get_name(line)
				names.append(name)
				replicas.append(replica)
				DLneeded-=2

setTouchableCommand = "/vis/set/touchable "
for name, replica in zip(reversed(names),reversed(replicas)):
	setTouchableCommand += " "+ name +" "+ replica
setTouchableCommand += "\n"
print("macro command to select the associated touchable:")
print(setTouchableCommand)
			
f = open("tmp.mac", "w")
f.write("/control/execute adultMIRDMale.mac \n")
f.write("/vis/ASCIITree/verbose 14 \n")
f.write("/vis/drawTree "+storePhysVolName+" \n")
f.write("/vis/viewer/flush \n")
f.write(setTouchableCommand)
f.write("/vis/touchable/dump \n")
f.close()

fout=[]
rootfile=[]

if args.outfile:
	outfile = args.outfile
	outfiles = outfile.split(".")
	if args.numMacros == 1: # Normal case of a single macro
		rootfile.append(outfile.replace(".mac", ""))
		if len(outfiles)<2 :
			# rootfile = outfile
			outfile += ".mac"
		# else:
		#     rootfile = outfiles[0]
		print("The output will be on",outfile)
		fout.append(open(outfile, "w"))
	else: # Case with multiple macros to simulate detector movement
		for numFile in range(0,int(args.numMacros)):
			outFileName=outfile.replace(".mac", "")+"_S"+str(args.smearing)+"_"+str(numFile)
			rootfile.append(outFileName)
			fout.append(open(outFileName+".mac", "w"))


	
if args.energyPrim:
	beamEne = args.energyPrim
	print("The beam energy (gamma) will be ",beamEne)

if args.loadGeom:
	loadGeomFlag=True
	print("The geometry will be loeaded too")

if args.nPrim:
	numPrimaries=args.nPrim
	print("The number of primaries will be ", numPrimaries)

print("running Geant4 to find", args.organ,"volume properties")
MyOut = subprocess.Popen(['./phantom', 'tmp.mac'], 
						 stdout=subprocess.PIPE, 
						 stderr=subprocess.STDOUT)
stdout,stderr = MyOut.communicate()

decodedout = stdout.decode("utf-8")

C = 1000000
xmin = C
ymin = C
zmin = C
xmax = -C
ymax = -C
zmax = -C

trigger = False

for iline, line in enumerate(decodedout.split('\n')):    
	# xyz(1)=18.5 39.1 700
	if args.verbose:
		print(iline, line)
	if line=="Global polyhedron coordinates:" :
		trigger = True
		if args.verbose:
			print(iline, "Enabling trigger")
	if re.match('^xyz*', line) and trigger:
		if args.verbose:
			print(iline, "matches")
		lines = line.split('=')
		if args.verbose:
			print(iline, lines)            
		# if len(lines)!=2: continue
		data = lines[1].split()
		print(data)
		if args.verbose:
			print(iline, data)                    
		data = [ float(x) for x in data ]
		if args.verbose:
			print(iline, data)                            
		if data[0]<xmin: xmin=data[0]
		if data[0]>xmax: xmax=data[0]
		if data[1]<ymin: ymin=data[1]
		if data[1]>ymax: ymax=data[1]
		if data[2]<zmin: zmin=data[2]
		if data[2]>zmax: zmax=data[2]    
if args.verbose:
	print(xmin,ymin,zmin)
	print(xmax,ymax,zmax)
halfx   = (xmax-xmin)/2
xcentre = (xmax+xmin)/2
halfy   = (ymax-ymin)/2
ycentre = (ymax+ymin)/2
halfz   = (zmax-zmin)/2
zcentre = (zmax+zmin)/2

halfx   = math.ceil(halfx)
xcentre = round(xcentre,2)
halfy   = math.ceil(halfy)
ycentre = round(ycentre,2)
halfz   = math.ceil(halfz)
zcentre = round(zcentre,2)



for numFile in range(0,int(args.numMacros)):

	outbuffer = ""
	if args.numMacros != 0: #If I asked for several macros means I want to move the detector
		outbuffer += ("/detector/setMaxDetPosErr " + str(args.smearing)+ " cm \n")

	if loadGeomFlag:
		outbuffer += ("/control/execute adultMIRDMale.mac \n")
	
	outbuffer += ("# \n")        
	outbuffer += ("### Radiation field defined \n")
	outbuffer += ("### by means of GPS \n")
	outbuffer += ("# \n")    
	outbuffer += ("/gps/verbose 0 \n")
	outbuffer += ("/gps/particle gamma \n")
	#outbuffer += ("/gps/energy 364 keV \n")
	outbuffer += ("/gps/energy "+str(beamEne) +" keV \n")
	outbuffer += ("# \n")
	outbuffer += ("/gps/pos/type Volume \n")
	outbuffer += ("/gps/pos/shape Para \n")
	outbuffer += ("/gps/pos/halfx "+str(halfx)+" mm \n")
	outbuffer += ("/gps/pos/halfy "+str(halfy)+" mm \n")
	outbuffer += ("/gps/pos/halfz "+str(halfz)+" mm \n")
	outbuffer += ("/gps/pos/centre "+str(xcentre)+" "+str(ycentre)+" "+str(zcentre)+" mm \n")
	outbuffer += ("/gps/pos/confine "+storePhysVolName+" \n")
	outbuffer += ("# \n")    
	outbuffer += ("/gps/ang/type iso \n")
	outbuffer += ("# \n")
	outbuffer += ("/analysis/setFileName "+rootfile[numFile]+"\n")
	if loadGeomFlag:
		outbuffer += ("/run/beamOn "+ str(numPrimaries) +" \n")

	print(outbuffer)

	if args.outfile:
		fout[numFile].write(outbuffer)
		print(fout[numFile].name,"file wrote")
		fout[numFile].close()
		if args.run:
			os.system('bsub ./phantom ' + fout[numFile].name)

