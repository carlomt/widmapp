#include "MyDetectorBuilder.hh"
#include "G4SystemOfUnits.hh"
#include "G4Element.hh"
#include "G4Material.hh"
#include "G4Tubs.hh"
#include "G4PVPlacement.hh"
#include "G4LogicalVolume.hh"
#include "G4Color.hh"
#include "G4VisAttributes.hh"
#include "G4Transform3D.hh"

MyDetectorBuilder::MyDetectorBuilder() :
  nDet(0)
{
  motherVolume = nullptr;

  G4double A;  // atomic mass
  G4double Z;  // atomic number
  
  A = 1.01*g/mole;
  G4Element* elH = new G4Element ("Hydrogen","H",Z = 1.,A);
  
  A = 12.011*g/mole;
  G4Element* elC = new G4Element("Carbon","C",Z = 6.,A);  

  G4Material* terphenyl = new G4Material("Terphenyl", 1.24*g/cm3, 2);
  terphenyl->AddElement(elH, 14);
  terphenyl->AddElement(elC, 18);

  fMaterial = terphenyl;
}

MyDetectorBuilder::~MyDetectorBuilder()
{
}

void MyDetectorBuilder::SetMotherVolume(G4VPhysicalVolume* mother)
{
  motherVolume = mother;
}

G4VPhysicalVolume* MyDetectorBuilder::GetVolume()
{
  return motherVolume;
}

G4VPhysicalVolume* MyDetectorBuilder::BuildDetector(const G4ThreeVector& position, G4RotationMatrix& rot)
{
  G4cout << "Building detector" << nDet << G4endl;  
  
  G4double radius = 26.*mm / 2;
  G4double h = 3.*mm;
  G4Tubs* det = new G4Tubs("Detector-"+std::to_string(nDet), 0., radius, h, 0., 2.*CLHEP::pi);

  G4LogicalVolume* logicDet = new G4LogicalVolume(det, fMaterial,
						  // "logicalDetector"+std::to_string(nDet));
						  "logicalDetector");  

  G4VPhysicalVolume* physDet = new G4PVPlacement( G4Transform3D(rot, position),
						 // "physicalDetector-"+std::to_string(nDet),
						  "physicalDetector",
						  logicDet, motherVolume,
						 true, nDet, true);

  G4cout << "Volume of Detector = " << logicDet->GetSolid()->GetCubicVolume()/cm3 << G4endl;

  G4Color red(1.0, 0.0, 0.0);
  // G4bool wireFrame = false; 
  G4VisAttributes* VisAtt = new G4VisAttributes(red);
  VisAtt->SetVisibility(true);  
  VisAtt->SetForceSolid(true);
  logicDet->SetVisAttributes(VisAtt);

  nDet++;

  return physDet;
}
