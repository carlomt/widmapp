//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// Previous authors: G. Guerrieri, S. Guatelli and M. G. Pia, INFN Genova, Italy
// Authors (since 2007): S. Guatelli, University of Wollongong, Australia
//
//
#include "G4HumanPhantomSteppingAction.hh"
#include "G4SteppingManager.hh"
#include "G4UnitsTable.hh"
#include "G4HumanPhantomConstruction.hh"
#include "G4HumanPhantomAnalysisManager.hh"
#include "G4SystemOfUnits.hh"

G4HumanPhantomSteppingAction::G4HumanPhantomSteppingAction(G4HumanPhantomConstruction* det, G4HumanPhantomAnalysisManager* an) :
  fDetector(det),
  fAnalysis(an)
{ }

G4HumanPhantomSteppingAction::~G4HumanPhantomSteppingAction()
{}

void G4HumanPhantomSteppingAction::UserSteppingAction(const G4Step* aStep)
{
  // G4cout<<"G4HumanPhantomSteppingAction::UserSteppingAction "<<__LINE__<<G4endl;
  G4VPhysicalVolume* prev_volume = aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume();
  G4VPhysicalVolume* post_volume = aStep->GetPostStepPoint()->GetTouchableHandle()->GetVolume();
 
  if(prev_volume == post_volume) return;
  // G4cout<<"G4HumanPhantomSteppingAction::UserSteppingAction "<<__LINE__<<G4endl;
  // G4cout<<"G4HumanPhantomSteppingAction::UserSteppingAction "<<post_volume->GetName()<<G4endl;

  for(const auto& det : fDetector->GetDetectorVolumes())
    {
      if(post_volume == det)
	{
#ifdef ANALYSIS_USE
	  // Fill Ntuple
	  fAnalysis->FillNtupleDetector(post_volume->GetCopyNo(), aStep->GetTrack()->GetKineticEnergy()/keV, aStep->GetTrack()->GetDynamicParticle()->GetPDGcode());
//

#endif
	  //G4cout<<post_volume->GetCopyNo()<<G4endl;
	  //G4cout<<"G4HumanPhantomSteppingAction::UserSteppingAction "<<aStep->GetTrack()->GetParticleDefinition()->GetParticleName()<<G4endl;
	  G4cout<<"G4HumanPhantomSteppingAction::UserSteppingAction "<<aStep->GetTrack()->GetDynamicParticle()->GetPDGcode()<<G4endl;

	  G4cout<<"G4HumanPhantomSteppingAction::UserSteppingAction "<<aStep->GetPostStepPoint()->GetCharge()<<G4endl;

	  // aStep->GetTrack()->GetDynamicParticle()->GetPrimaryParticle()->GetVertexPosition();
	}
    }
  // G4double edep = aStep->GetTotalEnergyDeposit();
  // aStep->GetTrack()->GetDefinition();
  
}
