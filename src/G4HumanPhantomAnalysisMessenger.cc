#include "G4HumanPhantomAnalysisMessenger.hh"
#include "G4HumanPhantomAnalysisManager.hh"
#include "G4HumanPhantomAnalysisHelper.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"

G4HumanPhantomAnalysisMessenger::G4HumanPhantomAnalysisMessenger(G4HumanPhantomAnalysisManager* m) :
  fAnalysisManager(m),
  fHelper(nullptr)
{
  fAnalysisManagerDir = new G4UIdirectory("/analysis/");
  fSetFileName = new G4UIcmdWithAString("/analysis/setFileName",this);
  fSetFileName->SetParameterName("setFileName",false);
  fSetFileName->SetToBeBroadcasted(false);

  fHelper = G4HumanPhantomAnalysisHelper::GetInstance();
}

G4HumanPhantomAnalysisMessenger::~G4HumanPhantomAnalysisMessenger()
{
  delete fAnalysisManagerDir;
  delete fSetFileName;
}

void G4HumanPhantomAnalysisMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if (command == fSetFileName )
    {
      G4cout<<"G4HumanPhantomAnalysisMessenger: setting output filename "<<newValue<<G4endl;
      // fAnalysisManager->SetOutputFilename(newValue);
      // fOutputFilename = newValue;
      fHelper->SetFileName(newValue);
    } 
  else
    {
      G4cerr << "***** Command is not found !!! " << newValue << G4endl;
    }

}

// G4String G4HumanPhantomAnalysisMessenger::GetOutputFileName()
// {
//   G4cout<<"G4HumanPhantomAnalysisMessenger::GetOutputFileName "<<fOutputFilename<<G4endl;
//   return fOutputFilename;
// }
