//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
/*
Author: Susanna Guatelli, University of Wollongong, Australia
*/
// The class G4HumanPhantomAnalysisManager creates and manages ntuples

// The analysis was included in this application following the extended Geant4
// example analysis/AnaEx01

#include <stdlib.h>
#include "G4HumanPhantomAnalysisManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

#include "G4HumanPhantomAnalysisHelper.hh"

G4HumanPhantomAnalysisManager::G4HumanPhantomAnalysisManager() :
  factoryOn(false),
  fMessenger(nullptr)
{
// Initialization ntuple
  for (G4int k=0; k<MaxNtCol; k++)
    {
      fNtColId[k] = 0;
    }
  for (G4int k=0; k<MaxNtCol; k++)
    {
      fNtColId2[k] = 0;
    }


  fMessenger = new G4HumanPhantomAnalysisMessenger(this);
}

G4HumanPhantomAnalysisManager::~G4HumanPhantomAnalysisManager() 
{ 
}

// void G4HumanPhantomAnalysisManager::SetOutputFilename(const G4String fn)
// {
//   fOutputFilename=fn;
//   G4cout<<"G4HumanPhantomAnalysisManager::SetOutputFilename "<<fOutputFilename<<G4endl;    
// }


void G4HumanPhantomAnalysisManager::book() 
{  
  G4AnalysisManager* AnalysisManager = G4AnalysisManager::Instance();
  
  AnalysisManager->SetVerboseLevel(2);
  AnalysisManager->SetNtupleMerging(true);
  // Create a root file
  // G4String fileName = "human_phantom";

  // // Create directories  
  AnalysisManager->SetNtupleDirectoryName("ntuple");

  // G4String OutputFilename = fMessenger->GetOutputFileName();
  G4String OutputFilename = G4HumanPhantomAnalysisHelper::GetInstance()->GetFileName();

  G4bool fileOpen = AnalysisManager->OpenFile(OutputFilename);
  if (!fileOpen)
    {
      G4cerr << "\n---> HistoManager::book(): cannot open " 
	     << OutputFilename
	     << G4endl;
      G4cout << "\n---> HistoManager::book(): cannot open " 
	     << OutputFilename
	     << G4endl;      
      return;
    }
  G4cout<<OutputFilename<<" output file opened."<<G4endl;

  //creating a ntuple, containg 3D energy deposition in the phantom
  AnalysisManager->SetFirstNtupleId(1);

  AnalysisManager -> CreateNtuple("detectors", "detectors");
  fNtColId2[0] =   AnalysisManager->CreateNtupleIColumn("DetectorID");
  fNtColId2[1] =   AnalysisManager->CreateNtupleDColumn("E");
  fNtColId2[2] =   AnalysisManager->CreateNtupleIColumn("ParticleId");
  AnalysisManager->FinishNtuple();

  AnalysisManager -> CreateNtuple("edep", "3Dedep");
  fNtColId[0] = AnalysisManager->CreateNtupleDColumn("organID");
  fNtColId[1] = AnalysisManager->CreateNtupleDColumn("edep");
  AnalysisManager->FinishNtuple();

  factoryOn = true;

  G4cout << "\n----> Output file is open in " 
         << AnalysisManager->GetFileName() << "." 
         << AnalysisManager->GetFileType() << G4endl;
}

 
void G4HumanPhantomAnalysisManager::FillNtupleWithEnergyDeposition(G4int organ,G4double energyDep)
{
  if (energyDep !=0)
    {
      G4AnalysisManager* AnalysisManager = G4AnalysisManager::Instance();
      AnalysisManager->FillNtupleDColumn(2, fNtColId[0], organ);
      AnalysisManager->FillNtupleDColumn(2, fNtColId[1], energyDep);
      AnalysisManager->AddNtupleRow(2);  
      // G4cout << "Analysis: organ " << organ << " edep: "<< energyDep << G4endl;  
    }
}

void G4HumanPhantomAnalysisManager::FillNtupleDetector(G4int detID, G4double E, G4int PID)
{
  G4AnalysisManager* AnalysisManager = G4AnalysisManager::Instance();
  AnalysisManager->FillNtupleIColumn(1, fNtColId2[0], detID);
  AnalysisManager->FillNtupleDColumn(1, fNtColId2[1], E);
  AnalysisManager->FillNtupleIColumn(1, fNtColId2[2], PID);
  AnalysisManager->AddNtupleRow(1);
}

void G4HumanPhantomAnalysisManager::save() 
{  
 if (factoryOn) 
   {
    G4AnalysisManager* AnalysisManager = G4AnalysisManager::Instance();    
    AnalysisManager->Write();
    AnalysisManager->CloseFile();  
      
    delete G4AnalysisManager::Instance();
    factoryOn = false;
   }
}












