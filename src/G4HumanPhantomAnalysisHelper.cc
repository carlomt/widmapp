#include "G4HumanPhantomAnalysisHelper.hh"

G4HumanPhantomAnalysisHelper* G4HumanPhantomAnalysisHelper::fInstance = nullptr;

#include "G4AutoLock.hh"
namespace {
  G4Mutex singletonMutex = G4MUTEX_INITIALIZER;
}

G4HumanPhantomAnalysisHelper* G4HumanPhantomAnalysisHelper::GetInstance()
{
  G4AutoLock l(&singletonMutex);
  if ( !fInstance ) fInstance = new G4HumanPhantomAnalysisHelper();
  return fInstance;
}

void G4HumanPhantomAnalysisHelper::SetFileName(const G4String f)
{
  fName=f;
}

G4String G4HumanPhantomAnalysisHelper::GetFileName()
{
  G4AutoLock l(&singletonMutex);
  return fName;
}

G4HumanPhantomAnalysisHelper::G4HumanPhantomAnalysisHelper()
{
}

