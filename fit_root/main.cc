
#include "TROOT.h"
#include "TMinuit.h"
#include "TMath.h"
#include "TStopwatch.h"
#include "TCanvas.h"
#include "TGraphErrors.h"
#include "TAxis.h"

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h" 

#include <array>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>      // std::setprecision
#include <random>

constexpr size_t nOrgans = 6;
constexpr size_t nDetectors = 7;
constexpr size_t nTimeSteps = 100;

using arr = std::array<double,nOrgans>;
using arr2D = std::array<std::array<double,nOrgans+1>,nDetectors>;

double allresp[nDetectors][nTimeSteps];
double allresperr[nDetectors][nTimeSteps];
double times[nTimeSteps];
double errtimes[nTimeSteps];
arr2D mcout;

//the response of a detector (iDet) at a given time
Double_t sigondet(float x, const arr& A, const arr& tau, arr2D mcout, const size_t iDet)
{
  Double_t res = 0.;
  for(size_t jOrg=0; jOrg<6; jOrg++)
    {
      res += mcout[iDet][jOrg+1] * A[jOrg]*exp(-x/tau[jOrg]);
    }
  return res;
}

double chi2(const double*  params)
{
  // const double A0=xx[0];
  // const double A1=xx[1];
  // const double A2=xx[2];
  // const double A3=xx[3];
  // const double A4=xx[4];
  // const double A5=xx[5];
  // const double tau0=xx[6];
  // const double tau1=xx[7];
  // const double tau2=xx[8];
  // const double tau3=xx[9];
  // const double tau4=xx[10];
  // const double tau5=xx[11];
  double res = 0.;
  for(size_t i=0; i<nDetectors; i++)
    {
      for(size_t t=0; t<nTimeSteps; t++)
	{
	  const double T = times[t];
	  double teo = 0.;
      	  for(size_t j=0; j<nOrgans; j++)
	    {
	      teo += params[j]*exp(-T/params[j+nOrgans])*mcout[i][j+1];
	    }
	  res += (allresp[i][t]-teo)*(allresp[i][t]-teo)/allresperr[i][t]/allresperr[i][t];
	}
    }
  return res;
}

void fcn(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag) 
{
  double res = 0.;
  for(size_t i=0; i<nDetectors; i++)
    {
      for(size_t t=0; t<nTimeSteps; t++)
	{
	  const double T = times[t];
	  double teo = 0.;
      	  for(size_t j=0; j<nOrgans; j++)
	    {
	      teo += par[j]*exp(-T/par[j+nOrgans])*mcout[i][j+1];
	    }
	  res += (allresp[i][t]-teo)*(allresp[i][t]-teo)/allresperr[i][t]/allresperr[i][t];
	}
    }
  f = res;  
}

// , Double_t* par)

int main()
{
  const int verbosity = 0;
  const arr A0   = {750000000., 9000000., 9000000., 1000000., 60000000., 6000000.};
  const arr tau  = {1920.,      12.5,     12.5,     8.656,    10,        17.31};
  const double tau_dist   = .5;
  const double A0_dist    = .5;
  const double perc_smear = .2;
  const double t_max = 50.;
  const std::array<std::string,nDetectors> detNames = {"ThyroidLob1","ThyroidLob2", "RightKidney","LeftKidney", 
						       "Bladder","Liver","Spleen"};  
  const std::array<std::string,nOrgans> orgNames = {"Thyroid", "RightKidney","LeftKidney", 
						    "Bladder","Liver","Spleen"};  

  std::cout<<"minimizzatore per WidmAPP"<<std::endl;

  //inizializzo il generatore pseudorandom
  std::random_device rd;
  std::mt19937 gen(rd());

  //reading MC output file
  const std::string mcoutfile = "/home/carlo/francescan/MC-out.txt";
  std::ifstream infile(mcoutfile);
  
  std::string line;
  size_t i=0;
  while (std::getline(infile, line))
    {
      if(line.size()<1) continue;
      if(line[0]=='#') continue;
      std::istringstream iss(line);
      std::vector<double> vals;
      double val;
      size_t j=0;
      while(iss>>val)
	{
	  // if(j==0) continue;
	  mcout[i][j] = val;
	  if(verbosity > 1)
	    std::cout<<" "<<val;
	  j++;
	}
      if(verbosity > 1)	
	std::cout<<std::endl;
      i++;
    }  

  std::cout<<"checking the MC output"<<std::endl;
  std::cout << std::fixed;
  for(const auto& line : mcout)
    {
      for(const auto& val : line)
	{
	  std::cout<<"\t"<<std::setprecision(5)<<val;
	}
      std::cout<<std::endl;
    }
  std::cout << std::scientific;

  // compute all the detector response for the chosen times using the MC outputs
  //il tempo massimo e' due volte il tau piu' grande
  // const double t_max = 2.*(*std::max_element(tau.begin(),tau.end()));
  const double stepSize = t_max/nTimeSteps;
  // std::vector<double> times, errtimes;

  // std::cout<<"t_max: "<<t_max<<std::endl;
  // for(double t=0.; t<t_max; t+=100.)
  //   {
  //     times.push_back(t);
  //     errtimes.push_back(0.);
  //   }

  //generate signal uniformly distributed within the range 'perc_smear'
  TCanvas *c1 = new TCanvas("c1","multipads",800,1200);
  c1->Divide(2,4,0,0);
  std::array<TGraphErrors*,nDetectors> graphs;
  // std::array<std::vector<double>,nDetectors> allresp;
  // std::array<std::vector<double>,nDetectors> allresperr;
  
  for(size_t i=0; i<nDetectors; i++)
    {
      // std::cout<<" i: "<<i<<std::endl;
      // std::cout<<std::flush;
      // allresp[i].resize(times.size());
      // allresperr[i].resize(times.size());
      // std::cout<<"resized"<<std::endl;
      // std::cout<<std::flush;
      // for(const auto& t : times)
      for(size_t t=0; t<nTimeSteps; t++)
	{
	  const double T = t*stepSize; 
	  times[t]=T;
	  errtimes[t]=0;	  
	  allresp[i][t] = 0.;
	  for(size_t j=0; j<nOrgans; j++)
	    {
	      allresp[i][t] += A0[j]*exp(-T/tau[j])*mcout[i][j+1];
	    } 
	  //smearing poissoniano
	  allresperr[i][t] = std::sqrt(allresp[i][t]);
	  std::normal_distribution<> gaus{allresp[i][t], allresperr[i][t]};
	  allresp[i][t] = gaus(gen);
	  // std::cout<<"allresp["<<i<<"]["<<t<<"]   : "<<allresp[i][t]<<std::endl;
	  // std::cout<<"allresperr["<<i<<"]["<<t<<"]: "<<allresperr[i][t]<<std::endl;
	  //smearing uniforme
	  const double half_width = allresp[i][t]*perc_smear;
	  std::uniform_real_distribution<> unif(allresp[i][t]-half_width, allresp[i][t]+half_width);
	  allresperr[i][t] = std::sqrt(4.*half_width*half_width/12. + 
				       allresperr[i][t]*allresperr[i][t]);
	  allresp[i][t] = unif(gen);
	}
      graphs[i] = new TGraphErrors(nTimeSteps,
				   times,allresp[i],
				   errtimes,allresperr[i]);
      c1->cd(i+1);
      gPad-> SetLogy();
      graphs[i]->SetTitle(detNames[i].c_str());
      graphs[i]->GetXaxis()->SetLimits(-t_max*.1, t_max*1.1);
      // graphs[i]->GetYaxis()->SetLimits(1.e+3, 2.e+7);
      graphs[i]->GetXaxis()->SetTitle("t  [h]");
      graphs[i]->GetYaxis()->SetTitle("counts  [Hz]");
      graphs[i]->Draw("AP");
    }
  c1->SaveAs("c1.pdf");
  delete c1;
  c1 = nullptr;
  for(auto& graph : graphs)
    {
      delete graph;
      graph = nullptr;
    }
  
  // create minimizer giving a name and a name (optionally) for the specific                                                                          
  // algorithm                                                                                                                                        
  // possible choices are:                                                                                                                            
  //     minName                  algoName                                                                                                            
  // Minuit /Minuit2             Migrad, Simplex,Combined,Scan  (default is Migrad)                                                                   
  //  Minuit2                     Fumili2                                                                                                             
  //  Fumili                                                                                                                                          
  //  GSLMultiMin                ConjugateFR, ConjugatePR, BFGS,                                                                                      
  //                              BFGS2, SteepestDescent                                                                                              
  //  GSLMultiFit                                                                                                                                     
  //   GSLSimAn                                                                                                                                       
  //   Genetic 
  ROOT::Math::Minimizer* minimum = ROOT::Math::Factory::CreateMinimizer("Minuit2","");//"Fumili2");
  minimum->SetMaxFunctionCalls(1000000000); // for Minuit/Minuit2 
  minimum->SetMaxIterations(10000000);  // for GSL
  minimum->SetTolerance(0.1);  
  minimum->SetPrintLevel(1);

  // create function wrapper for minimizer
  ROOT::Math::Functor f(&chi2, nOrgans*2);

  // TMinuit *gMinuit = new TMinuit(nOrgans*2);
  // gMinuit->SetFCN(fcn);

  double step[nOrgans*2] = {.1,.1,.1,.1,.1,.1,
			    .1,.1,.1,.1,.1,.1,};

  minimum->SetFunction(f); 

  //generate an initial guess for the parameters uniformly distributed within a certain range
  arr A_guess;
  arr tau_guess;
  // Int_t ierflg = 0;
  Double_t arglist[nOrgans*2];  
  for(size_t i=0; i<nOrgans; i++)
    {
      A_guess[i] = std::generate_canonical<double, 64>(gen)*
      	2.*A0[i]*(1.+A0_dist)-A0[i]*(1.-A0_dist);
      tau_guess[i] = std::generate_canonical<double, 64>(gen)*
      	2.*tau[i]*(1.+tau_dist)-tau[i]*(1.-tau_dist);

      minimum->SetVariable(i,         "A0-" +orgNames[i], A_guess[i],   step[i]);
      minimum->SetVariable(i+nOrgans, "tau-"+orgNames[i], tau_guess[i], step[i+nOrgans]);

      // gMinuit->mnparm(i,         "A0-" +orgNames[i], A_guess[i],   step[i],         0,0,ierflg); 
      // gMinuit->mnparm(i+nOrgans, "tau-"+orgNames[i], tau_guess[i], step[i+nOrgans], 0,0,ierflg);
    }
  // do the minimization
  minimum->Minimize();
  // gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);  
  
  // Print results  
  // Double_t amin,edm,errdef;
  // Int_t nvpar,nparx,icstat;
  // gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);  
  
  const double *xs = minimum->X();
  

}
