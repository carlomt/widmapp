# -*- coding: utf-8 -*-

from __future__ import print_function, division
from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt

#from google.colab import files

#uploaded = files.upload()

def readfile(fname):
  output = []
  with open(fname) as f:
    next(f)
    for line in f:
      output.append(np.array(line.strip().split('\t')[1:],dtype='float64'))
  return output

MCout = readfile("MC-out.txt")

#total initial activity in MBq
A0 = np.array([1294.0,0.,0.,0.,1457.2,0.])
# radionuclide average life for different organs in hours
tau = np.array([277.679,0.1,0.1,0.1,8.656,0.1])

#modify MCout to increase cross-correlation
#MCout[1][1] = MCout[0][0]
#increase also activity of second organ
#A0[1] = 1000
#and tau
#tau[1] = 10

#the activity of all organs at a given time
def activity(t, A0, tau):
  return A0 * np.exp(-t/tau)

#the response of a detector (id) at a given time
def detector_response(id, t):
  return (activity(t,A0,tau)).dot(MCout[id])

# the times for the plot, from 0 h to 2*tau[0] (the largest)
# one point every 10 h
t = np.arange(0,  2*tau[0], 10)

# compute all the detector response for the chosen times
allr = []
for i in range(0,len(MCout)):
  r = []
  for thist in t:
    r.append(detector_response(i,thist))
  allr.append(r)

  # plot det responses as a function of time
  plt.plot(t, allr[i],'.', markersize=2, label='Det#%s'%i)
  plt.axis([0, 2*tau[0], 0.1, 35])
  plt.legend()
  plt.title('CPS vs t')
  plt.xlabel('t[h]')
  plt.ylabel('CPS')                                                                                                                                               
plt.grid(linestyle= '--')
plt.show()

def func(x, A0, tau0, A1, tau1, A2,tau2):
  return A0 * np.exp(-x/tau0) + A1 * np.exp(-x/tau1)+ A2*np.exp(-x/tau2)

#sum of Nexponential
def ExpSum(t, *arg):
  sum=0
  k=int(len(arg)/2)
  for i in range(k):
       sum +=arg[2*i]*np.exp(-t/arg[2*i+1])        
  return sum

res=[]
modelPred=[]

for Det in range (0,len(MCout)):
  #initial guess
  S0=MCout[Det]*A0
  print("S0 iniz",S0)
  print("MC iniz",MCout[Det])
  argv=np.column_stack((S0,tau)).flatten()

  #fit
  #popt, pcov = curve_fit(func, t, allr[Det], bounds=(0, [np.inf, np.inf, np.inf, np.inf, np.inf, np.inf]))
  popt, pcov = curve_fit(ExpSum, t, allr[Det], p0=argv, bounds=(0, [np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf]))
  #  print(" Optimal parameters (A00,tau0, A01, tau1, A02,tau2:",popt, " Optimal covariance:", pcov)
  #print(" Optimal parameters (S00,tau0, S01, tau1, S02,tau2, S03, tau3, S04, tau4, S05, tau5:",popt, " Optimal covariance:", pcov) 
  #  modelPred.append(func(t,*popt))

  Sfit=[]
  for k in range(6):
    Sfit.append(popt[2*k])
  print("S0 finale",Sfit)
  print("MC finale",MCout[Det])
  print(Sfit/MCout[Det])

  modelPred.append(ExpSum(t,*popt))                                                                                              
  res.append(allr[Det]-modelPred[Det])
  
  #plot residuals  
  plt.scatter(modelPred[Det], res[Det], c='b',marker='.',linewidths='0.5')                                          
  plt.legend()                                  
  plt.title('Residuals vs modelPredictions Det#:%s'%i)                                                                                       
  plt.xlabel('modelPredictions')                                                                                         
  plt.ylabel('Residuals')
  plt.show()
  plt.plot(t, allr[Det],'o', label='Det #%s'%i)
  #plt.plot(t,func(t, *popt), 'r--', label='fit: A0=%5.3f, tau0=%5.3f, A1=%5.3f, tau1=%5.3f, A2=%5.3f, tau2=%5.3f' % tuple(popt))
  plt.plot(t,  ExpSum(t, *popt), 'r--', label='fit: A0=%5.3f, tau0=%5.3f, A1=%5.3f, tau1=%5.3f, A2=%5.3f, tau2=%5.3f, A3=%5.3f, tau3=%5.3f, A4=%5.3f, tau4=%5.3f, A5=%5.3f, tau5=y%5.3f' % tuple(popt))
  plt.xlabel('t [h]')
  plt.ylabel('Detector counts')
  plt.legend()
  plt.show()

