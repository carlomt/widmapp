#!/usr/bin/env python
# coding: utf-8

from __future__ import division, print_function

import sys
sys.path.append('./')
import os.path

import readfile

from tqdm import tqdm
import multiprocessing
from joblib import Parallel, delayed

import numpy as np
from scipy.optimize import curve_fit
from scipy.optimize import minimize
from scipy.stats import chisquare

from common import num_representation, safe_num_representation_max
from common import OrganNames, A0s, taus, DetNames, MCout
from common import activity, det_response_teo, det_contributions, det_response
from common import det_matrix_smear as det_response_smear

from smearing_common import times
from smearing_common import widthsMatrix as widths 

chisq_reg = num_representation(1.e-6) 

np.seterr(over='ignore')

n_iterations=1000

num_cores = multiprocessing.cpu_count()
num_cores_used  = 10

x = np.concatenate((A0s,taus))

#the theoretical (without smearing) response of a detector (det_id)    
#redefined because of chisq
def det_response_teo2(times, x, det_id):
  return det_response_teo(times, x[:6],x[6:], det_id)

#generate an initial guess for the parameters uniformly distributed within a certain range
priorwidth = 50./100.
A_guess = np.random.uniform(A0s-A0s*priorwidth,A0s+A0s*priorwidth)
tau_guess = np.random.uniform(taus-taus*priorwidth,taus+taus*priorwidth)
x_0=np.concatenate((A_guess,tau_guess))

def fit(width,A0s,taus):
  #print("width:",width,"step:",n,"################")
  CPSall = np.empty(shape=(len(MCout),len(times)))
  errCPSall = np.empty(shape=(len(MCout),len(times)))
  for i in range(0,len(MCout)):
    resp = det_response_smear(times, A0s, taus, i, width)
    CPSall[i] = resp
    errCPSall[i] = np.sqrt(resp)
  #x0 contains 'initial guess' for the fit
  
  #print('Initial guess',x_0)
  def chisq(x):
    res=0
    for i in range(0,len(MCout)):
      res += np.sum( ( (CPSall[i]*chisq_reg - det_response_teo2(times,x,i)*chisq_reg )/errCPSall[i] )**2 )
    return res
  #fit with TNC method
  res_Nelder= minimize(chisq, x_0, method='Nelder-Mead',options={'maxiter': 12*3000, 'maxfev': 12*3000, 'adaptive': True})
  
  #print('RISULTATI TNC',res_Nelder.x)
  #print("res",(res_Nelder.x-x)/x)
  #res_chisq[n] = chisq(res_Nelder.x)*(10**6)
  #print("chisq:",res_chisq[n])
  
  return res_Nelder    


#%%time
if __name__ == "__main__":
    print("Entering main...", flush = True)
    print("widths:",widths)
    print("n_iterations:",n_iterations)
    print("Using",num_cores_used,"cores of",num_cores)
    print("Organ name","\t","tau","\t","A0")
    for OrganName, tau, A0 in zip(OrganNames,taus,A0s):
      print(OrganName,"\t",tau,"\t",A0)
    processed_lists = [[0]*n_iterations]*len(widths)
    for i, width in enumerate(widths):
      #for n in range(0,step_numb):
      for n in tqdm(range(n_iterations), leave = False):   
        print("width:",width,"step:",n,"################")
        processed_lists[i] = Parallel(n_jobs=num_cores_used)(delayed(fit)(
          width = width,
          A0s=A0s,
          taus=taus)
          for n in tqdm(range(n_iterations)))

np.save('smearingMatrix.npy', processed_lists) 

