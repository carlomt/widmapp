#!/usr/bin/env python
# coding: utf-8

from __future__ import division, print_function

import sys
sys.path.append('./')
import os.path

import numpy as np
from common import num_representation, det_response
from common import A0s, taus, MCout, times
#from common import activity, det_response_teo, det_contributions


widths = np.array([.1, .3, .5, .7, .9, 1.1], dtype=num_representation)
# widths = np.array([.1, .3, .6, .9, 1.2, 1.5 ], dtype=num_representation)
# widths = np.array([.9,  1.], dtype=num_representation)
#times = np.arange(0,48,1,dtype=num_representation) #two days, step 1 hour
# times1 = np.arange(0,48,.2, dtype=num_representation) #two days, step 12 mins
# times2 = np.arange(48,240,1, dtype=num_representation) #10 days, step 1 hour
# times = np.concatenate((times1,times2))

#compute the response of all detector to speed up the fit
CPSall = np.empty(shape=(len(MCout),len(times)))
errCPSall = np.empty(shape=(len(MCout),len(times)))
for i in range(0,len(MCout)):
    resp = det_response(times, A0s, taus, i)
    CPSall[i] = resp
    errCPSall[i] = np.sqrt(resp)

errCPSall += num_representation(1.e-6) #to avoid zeros in the denominator of chi2
