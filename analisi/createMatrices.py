from __future__ import division, print_function
import glob
import os
import uproot
import numpy as np
import awkward as ak

print("uproot version:",uproot.__version__)
print("awkward version:",ak.__version__)
NPrimMC = 1.e+7
organNames = ("Thyroid","rKidney","lKidney","Bladder","Liver","Spleen")
detectorNames = ("DetThyroid1","DetThyroid2","DetKidneyR",
                 "DetKidneyL","DetBladder","DetLiver","DetSpleen")
folder="/home/carlo/repos/widmapp/DatiRoot"
filenames = glob.glob(folder+"/*_S0_?.root")
filenames2 = glob.glob(folder+"/*_S0_??.root")
filenames += filenames2
# print(filenames)
nrun = int(len(filenames)/len(organNames))
allcounts = np.empty(shape=(nrun,len(detectorNames),len(organNames)))
uniques = np.empty_like(allcounts)
for filename in filenames:
    print("Working on:",filename)
    filenamesplitted = os.path.basename(filename).split('_')
    i = int(filenamesplitted[2].split('.')[0])
    k = organNames.index(filenamesplitted[0])
    print("i:",i,"k:",k)
    file = uproot.open(filename)
    tree = file['ntuple/detectors']
    array = tree['row_wise_branch'].array()
    a = ak.to_numpy(array["DetectorID"])
    uniques[i,:,k], allcounts[i,:,k] = np.unique(a, return_counts=True)
    allcounts[i,:,k] /= NPrimMC

means = np.empty(shape=(len(detectorNames),len(organNames)))
sigmas = np.empty_like(means) 
for j in range(len(detectorNames)):
    for k in range(len(organNames)):
        means[j,k] = np.mean(allcounts[:,j,k])
        sigmas[j,k] = np.std(allcounts[:,j,k])

print("means =",means,"\n")
print("sigmas =",sigmas,"\n")

with open("MC-out-auto.txt", 'w') as outfile:
    outfile.write('#\t')
    for organ in organNames: 
        outfile.write(organ+'\t')
    outfile.write('\n')
    for i, line in enumerate(means):
        outfile.write(str(i)+'\t')
        for mean in line:
            outfile.write(str(mean)+'\t')
        outfile.write('\n')    
