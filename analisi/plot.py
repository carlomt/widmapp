#!/usr/bin/env python
# coding: utf-8

from __future__ import division, print_function

import numpy as np

import sys
sys.path.append('./')
import os.path

import matplotlib.pyplot as plt
#import matplotlib.colors as mcolors

from common import basecolors, lightcolors, darkcolors
from common import OrganNames, A0s, taus, DetNames
from common import activity, det_response_teo, det_contributions, det_response

def plot_det(organid, times, A0s,taus, MCout, CPSall, 
             ylim=(-1.2,1.2),
             outputfilename=None):
  i = organid
  y=det_response_teo(times,A0s,taus,i)
  y_smeared=CPSall[i]
  contributions=det_contributions(times, A0s, taus, i)
  fig1 = plt.figure(1)
  frame1=fig1.add_axes((.1,.3,.8,.6))
  plt.grid()
  plt.errorbar(times, y_smeared, np.sqrt(y_smeared),None, 'o', markersize=4, label='Generated data',color=basecolors[0])
  plt.plot(times, y, 'r-', markersize=2.5,label='Theoretical %s \ndetector response' %DetNames[i],color=basecolors[1])
  for k, (contribution, name) in enumerate(zip(contributions.T,OrganNames)):
    plt.plot(times,contribution,'--',label=name,color=darkcolors[k])
  #plt.title(' Det on %s \n Signal vs t[h]' %DetNames[i])
  plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
  #plt.xlim(0,700)
  plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
  plt.gca().yaxis.get_offset_text().set_x(-0.11)
  frame1.set_ylabel('Signal [cps]')
  residual=(y_smeared-y)/y*100
  #plt.ylim([-100000, 100000])
  #residual=y-y_smeared[n][Det]
  frame2=fig1.add_axes((.1,.1,.8,.2))      
  plt.plot(times, residual, 'o', markersize=3)
  plt.axhline(y=0., xmin=0, xmax=times[-1], color='r')
  frame2.set_ylabel('Res. [%]')
  #plt.ylim([-9000, 9000])
  plt.ticklabel_format(style='plain', axis='y', scilimits=(0,0))
  plt.xlabel('t [h]')
  plt.gca().yaxis.get_offset_text().set_x(-0.11)
  print('RMS',np.sqrt(np.mean(residual**2)))
  #plt.yscale('log')
  #plt.xlim(0,700)
  plt.ylim(ylim)
  plt.grid()
  plt.show()
  if outputfilename:
    plt.savefig(outputfilename)


def plot_fit(organid, fit_output, times, A0s,taus, MCout, CPSall,
             ylim=(-1.2,1.2), outputfilename=None):
  i = organid
  y=det_response_teo(times,fit_output.x[:6],fit_output.x[6:],i)
  y_smeared=CPSall[i]
  contributions=det_contributions(times, A0s, taus, i)
  fit_contributions=det_contributions(times, fit_output.x[:6],fit_output.x[6:], i,)
  fig1 = plt.figure(1)
  frame1=fig1.add_axes((.1,.3,.8,.6))
  plt.grid()
  plt.errorbar(times, y_smeared, np.sqrt(y_smeared),None, 'o', markersize=4, label='Generated data',color=basecolors[0])
  plt.plot(times, y, 'r-', markersize=2.5,label='Fit output for the %s \ndetector' %DetNames[i],color=basecolors[1])
  for k, (fit_contribution, name) in enumerate(zip(fit_contributions.T,OrganNames)):
    plt.plot(times,fit_contribution,'-',label='fit %s'%name,color=lightcolors[k])
  for k, (contribution, name) in enumerate(zip(contributions.T,OrganNames)):
    plt.plot(times,contribution,'--',label=name,color=darkcolors[k],linewidth=3)
  #plt.title(' Det on %s \n Signal vs t[h]' %DetNames[i])
  plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
  #plt.xlim(0,700)
  plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
  plt.gca().yaxis.get_offset_text().set_x(-0.11)
  frame1.set_ylabel('Signal [cps]')
  residual=(y_smeared-y)/y*100
  #plt.ylim([-100000, 100000])
  #residual=y-y_smeared[n][Det]
  frame2=fig1.add_axes((.1,.1,.8,.2))      
  plt.plot(times, residual, 'o', markersize=3)
  plt.axhline(y=0., xmin=0, xmax=times[-1], color='r')
  frame2.set_ylabel('Res. [%]')
  #plt.ylim([-9000, 9000])
  plt.ticklabel_format(style='plain', axis='y', scilimits=(0,0))
  plt.xlabel('t [h]')
  plt.gca().yaxis.get_offset_text().set_x(-0.11)
  print('RMS',np.sqrt(np.mean(residual**2)))
  #plt.yscale('log')
  #plt.xlim(0,700)
  plt.ylim(ylim)
  plt.grid()
  plt.show()
  if outputfilename:
    plt.savefig(outputfilename)
