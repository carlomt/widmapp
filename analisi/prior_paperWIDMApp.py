#!/usr/bin/env python
# coding: utf-8

from __future__ import division, print_function

import sys
sys.path.append('./')
import os.path

import readfile

from tqdm import tqdm
import multiprocessing
from joblib import Parallel, delayed

import numpy as np
from scipy.optimize import curve_fit
from scipy.optimize import minimize
from scipy.stats import chisquare
from scipy.optimize import leastsq

from common import num_representation, safe_num_representation_max
from common import OrganNames, A0s, taus, DetNames, MCout
from common import activity, det_response_teo, det_contributions, det_response, bounds

from prior_common import widths, times, CPSall, errCPSall

chisq_reg = num_representation(1.e-6)

#np.seterr(all='log')
np.seterr(over='ignore')

n_iterations=100

num_cores_used  = 10
num_cores = multiprocessing.cpu_count()

x = np.concatenate((A0s,taus))

#the theoretical (without smearing) response of a detector (det_id)    
#redefined because of chisq
def det_response_teo2(times, x, det_id):
  return det_response_teo(times, x[:6],x[6:], det_id)

def chisq(x):
  res=0
  # if np.any((x<0)|(x>1250)):
  # if np.any(x<0):
  #   # print("###############################################")
  #   # print("returning inf")
  #   # print("x:",x)
  #   # print("###############################################")
  #   return np.inf
  for i in range(0,len(MCout)):
    res += np.sum( ( (CPSall[i]*chisq_reg - det_response_teo2(times,x,i)*chisq_reg )/errCPSall[i] )**2 )
    # if (res>safe_num_representation_max).any():
    #   print("WARNING","chisq","res:",res,"i:",i)
    #   if (np.isinf(res).any()):
    #     print("#########################################")
    #     print("#########################################")
    #     print("CPSall[i]:",CPSall[i])
    #     print("#########################################")
    #     print("det_response_teo2(times,x,i):",det_response_teo2(times,x,i))
    #     print("#########################################")
    #     print("x A0s:",x[:6])
    #     print("x taus:",x[6:])
    #     print("#########################################")
    #     print("errCPSall[i]:",errCPSall[i])
    #     print("#########################################")        
    #     print("#########################################")
    #     raise
  #res=res*(10**(-6)) meglio moltiplicare ogni elemento per 10^-6 in modo da evitare overflows
  # print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
  # print("returning",res)
  # print("x:",x) 
  # print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
  return res


def fit(width,A0s,taus,n=0):
  #
  #generate an initial guess for the parameters uniformly distributed within a certain range
  A_guess = np.random.uniform(A0s-A0s*width,A0s+A0s*width)
  tau_guess = np.random.uniform(taus-taus*width,taus+taus*width)
  #x0 contains 'initial guess' for the fit
  x_0=np.concatenate((A_guess,tau_guess))
  print('n:',n,'\tInitial guess:\t',x_0)
  
  #fit
  res_Nelder= minimize(chisq, x_0, method='Nelder-Mead',options={'maxiter': 12*3000, 'maxfev': 12*3000, 'adaptive': True})
  # res_TNC = minimize(chisq, x_0, method='TNC', jac=chisq_derivative, bounds=bounds)
  # res_Nelder= leastsq(chisq, x_0)
  # res_Nelder= minimize(chisq, x_0, method='L-BFGS-B', bounds=bounds)
  # res_Nelder= minimize(chisq, x_0,  bounds=bounds)
  # res_Nelder= minimize(chisq, x_0, method='BFGS')
  
  print('n:',n,'\tfit output:\t',res_Nelder.x)
  # print("res",(res_Nelder.x-x)/x)
  #res_chisq[n] = chisq(res_Nelder.x)*(10**6)
  #print("chisq:",res_chisq[n])
  
  return res_Nelder    
  


#%%time
if __name__ == "__main__":
    print("Entering main...", flush = True)
    print("widths:",widths)
    print("n_iterations:",n_iterations)
    print("Using",num_cores_used,"cores of",num_cores)
    print("Organ name","\t","tau","\t","A0")
    for OrganName, tau, A0 in zip(OrganNames,taus,A0s):
      print(OrganName,"\t",tau,"\t",A0)
    processed_lists = [[0]*n_iterations]*len(widths)
    for i, width in enumerate(widths):
      #for n in range(0,step_numb):
      # for n in tqdm(range(n_iterations), leave = False):   
      # print("width:",width,"step:",n,"################")
      processed_lists[i] = Parallel(n_jobs=num_cores_used)(delayed(fit)(
        width = width,
        A0s=A0s,
        taus=taus,
        n=n)
        for n in tqdm(range(n_iterations)))

#np.savez_compressed('prior.npz', processed_lists)
np.save('prior3.npy', processed_lists)

# for i, processed_list in enumerate(processed_lists):
#     print("len(processed_list)",len(processed_list))
#     for n, res_Nelder in enumerate(processed_list):
#         if i==0 and n==0 :
#             print("i:",i)
#             #print(res_Nelder)
#             print(type(res_Nelder))
#             print(res_Nelder.x)


# # res_chisq_means = np.empty(len(widths))
# # res_chisq_vars = np.empty(len(widths))
# residuiA0_means = np.empty(shape=(len(widths),len(A0s)))
# residuiA0_vars = np.empty(shape=(len(widths),len(A0s)))
# residuiTau_means = np.empty(shape=(len(widths),len(taus)))
# residuiTau_vars = np.empty(shape=(len(widths),len(taus)))
# for i, processed_list in enumerate(processed_lists): #loop sulla percentuale di semaring
#     # res_chisq = np.empty(len(processed_list))
#     residui = np.empty(shape=(len(processed_list),len(A0s)+len(taus)))
#     for n, res_Nelder in enumerate(processed_list): #loop sui tentativi di fit per ogni smearing
#     #     print("i:",i,"n:",n)
#     #     print('RISULTATI TNC',res_Nelder.x)
#         residui[n] = (res_Nelder.x-x)/x*100
#     #     print("residui",residui,"%")
#     #     res_chisq[n] = chisq(res_Nelder.x)*(10**6)
#     #     print("chisq:",res_chisq[n])
#     # print("################","end loop","#################")    
#     #RMS=np.sqrt(np.sum(res_chisq**2)/step_numb)

#     # res_chisq_means[i]=np.mean(res_chisq)
#     # res_chisq_vars[i]=np.var(res_chisq)    
#     #print('var',res_chisq_vars[i])
#     #print('chisq mean',res_chisq_means[i])
#     for j in range(len(A0s)):
#         residuiA0_means[i][j] = np.mean(residui[:,j])
#         residuiA0_vars[i][j] = np.std(residui[:,j])
#         residuiTau_means[i][j] = np.mean(residui[:,j+len(A0s)])
#         residuiTau_vars[i][j] = np.std(residui[:,j+len(A0s)])


# # plt.errorbar(widths, res_chisq_means, res_chisq_vars, None, '.', label='Residual chi2')
# # plt.ylim(1.e-1,1.e+3)
# # #plt.yscale('log')
# # plt.gca().set_ylabel(r'$\chi^2_{res}$')
# # plt.gca().set_xlabel("Prior sampling width")
# # plt.savefig("output/chi2prior.png")


# fig1 = plt.figure(1)
# frame1=fig1.add_axes((.1,.71,.8,.6))
# for j in range(len(A0s)):
#     x = np.asarray(widths)*100
#     x = x+j
#     plt.errorbar(x, residuiA0_means[:,j], residuiA0_vars[:,j], None, 'o', label=OrganNames[j],color=lightcolors[j])
# plt.ylim(-45,45)
# plt.grid()
# #plt.yscale('log')
# plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
# plt.gca().set_ylabel(r'Residual $A_0$ [%]')

# frame2=fig1.add_axes((.1,.1,.8,.6))      
# for j in range(len(taus)):
#     x = np.asarray(widths)*100
#     x = x+j
#     plt.errorbar(x, residuiTau_means[:,j], residuiTau_vars[:,j], None, 'o', label=OrganNames[j],color=lightcolors[j])
# plt.ylim(-45,45)
# plt.grid()
# #plt.yscale('log')
# #plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
# plt.gca().set_ylabel(r'Residual $\tau$ [%]')
# plt.gca().set_xlabel("Prior sampling half-width [%]")
# plt.savefig("output/prior.png")




# #i = 2 #right kidney
# fit_output = processed_lists[0][0] #il primo numero e' per decidere quale larghezza di campionamento prendere per il plot

# plot_fit(2,fit_output, times, A0s,taus, MCout, CPSall, "output/fit.png")
