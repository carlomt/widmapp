#!/usr/bin/env python
# coding: utf-8

from __future__ import division, print_function

import numpy as np
import matplotlib.colors as mcolors

import readfile

MCout = readfile.readfile('MC-out.txt')

np.seterr(over='ignore')
num_representation = np.float64

safe_num_representation_max = np.finfo(num_representation).max*(0.01)

basecolors = [mcolors.CSS4_COLORS['blue'],
               mcolors.CSS4_COLORS['orange']]
lightcolors= [mcolors.CSS4_COLORS['red'],
               mcolors.CSS4_COLORS['limegreen'],
               mcolors.CSS4_COLORS['violet'],
               mcolors.CSS4_COLORS['sandybrown'],
               mcolors.CSS4_COLORS['pink'],
               mcolors.CSS4_COLORS['cyan'],
               mcolors.CSS4_COLORS['gray']
]
darkcolors= [mcolors.CSS4_COLORS['darkred'],
               mcolors.CSS4_COLORS['darkgreen'],
               mcolors.CSS4_COLORS['purple'],
               mcolors.CSS4_COLORS['sienna'],
               mcolors.CSS4_COLORS['palevioletred'],
               mcolors.CSS4_COLORS['cadetblue'],
               mcolors.CSS4_COLORS['black']
]

OrganNames= [
    "Thyroid", 
    "Right Kidney",
    "Left Kidney", 
    "Bladder",
    "Liver",
    "Spleen",
#    "Trunk"
]

t_bio= np.array([
1920., #Thyroid
12.,   #Right Kidney
10.,   #Left Kidney
7.,    #Bladder
24.,   #Liver
17.,   #Spleen
#5.     #Trunk/blood
], dtype=num_representation) #hours


t_I131=192.4728
t_eff=1./(1./t_I131+1./t_bio)

#A0s= np.array([750,9,9,1,60,6], dtype=num_representation)*1.e+6 #Bq
#A0s= np.array([750, 127.967, 128.197, 40.6156, 1296.83, 156.391], dtype=num_representation)*1.e+6 #Bq
# A0s= np.array([
# 750,
# 10,
# 10,
# 2.,
# 60.,
# 6.,
# 1000.
# ], dtype=num_representation)*1.e+6 #Bq 
A0s= np.array([
750,  #Thyroid
115,   #Right Kidney
105,   #Left Kidney
35.,   #Bladder
1115.,  #Liver
130.,   #Spleen
#120.  #Trunk/blood
], dtype=num_representation)*1.e+6 #Bq 


#taus= np.array([1920,12.5,12.5,8.656,10.,17.31]) #hours
taus=t_eff/np.log(2.)

#detectors names
DetNames= ["Thyroid Lob 1","Thyroid Lob 2", "Right Kidney","Left Kidney", "Bladder","Liver","Spleen"]

times1 = np.arange(0,48,.2, dtype=num_representation) #two days, step 12 mins
times2 = np.arange(48,240,1, dtype=num_representation) #10 days, step 1 hour
times = np.concatenate((times1,times2))

A0min = 1
A0max = 2500
taumin = 1
taumax = 2000
bounds=(
#A0s
    (A0min,A0max), #Thyroid 
    (A0min,A0max), #Right Kidney 
    (A0min,A0max), #Left Kidney
    (A0min,A0max), #Bladder
    (A0min,A0max), #Liver
    (A0min,A0max), #Spleen 
#    (A0min,A0max), #Trunk/blood  
#taus
    (taumin,taumax), #Thyroid 
    (taumin,taumax), #Right Kidney 
    (taumin,taumax), #Left Kidney
    (taumin,taumax), #Bladder
    (taumin,taumax), #Liver
    (taumin,taumax), #Spleen 
#    (taumin,taumax) #Trunk/blood  
)





#the activity of an organ at a given time
def activity(t, A0s, taus):
    res = np.empty(len(A0s))
    for i, (A0, tau) in enumerate(zip(A0s, taus)):        
        res[i] = A0*np.exp(-t/tau)
    return res    

#the theoretical (without smearing) response of a detector (det_id)
def det_response_teo(times, A0s, taus, det_id):
    res = np.zeros(len(times))
    for i, (A0, tau) in enumerate(zip(A0s, taus)):
        res += A0*np.exp(-times/tau)*MCout[det_id][i]
        # if (res>safe_num_representation_max*.1).any():
        #     print("----------------------------------------------")
        #     print("WARNING","det_response_teo","res:",res,"i:",i)
        #     print("A0s:",A0s)
        #     print("taus:",taus)
        #     print("----------------------------------------------")
    return res

#the contribution of each organ to the response of a detector (det_id)
def det_contributions(times, A0, tau, det_id):
    res = np.empty(shape=(len(times),len(A0)))   
    for i, time in enumerate(times):
        res[i] = activity(time,A0,tau)*MCout[det_id]
    return res


#the response of a detector (det_id) with poissonian smearing
def det_response(times, A0, tau, det_id):
    res = np.empty(len(times))
    for i, time in enumerate(times):
        tmp = activity(time,A0,tau).dot(MCout[det_id])
        res[i] = np.random.poisson(tmp)
    return res


#the response of a detector (det_id) with poissonian ad uniform smearing 
def det_response_smear(times, A0s, taus, det_id, width):
    #res = np.empty(len(times))
    #for i, time in enumerate(times):
    #    tmp = np.random.poisson(activity(time,A0,tau).dot(MCout[det_id]))
    #    res[i] = np.random.uniform(tmp*(1-width), tmp*(1+width))
    tmp = np.zeros(len(times))
    for i, (A0, tau) in enumerate(zip(A0s, taus)):
        tmp += A0*np.exp(-times/tau)*MCout[det_id][i]
    res = np.random.uniform(tmp*(1-width),tmp*(1+width))
    # print(res)
    return res

def det_matrix_smear(times, A0, tau, det_id, width=1.):
    MCoutMeans = np.array([
        [1.4299166e-02, 4.0853000e-05, 3.7124000e-05, 3.0120000e-06,
        7.7635000e-05, 7.2320000e-05],
       [1.4298926e-02, 3.7064000e-05, 4.1367000e-05, 2.9590000e-06,
        6.9951000e-05, 8.6565000e-05],
       [4.3793000e-05, 2.0782608e-02, 1.9242970e-03, 2.2154200e-04,
        2.6461940e-03, 7.5509400e-04],
       [4.5262000e-05, 1.9184860e-03, 2.0634857e-02, 2.2334100e-04,
        9.2310400e-04, 4.8681430e-03],
       [3.1450000e-06, 2.0047900e-04, 1.9737300e-04, 1.3739967e-02,
        1.6409000e-04, 1.1543300e-04],
       [6.0074000e-05, 1.7345030e-03, 5.8404600e-04, 1.6286100e-04,
        6.0498280e-03, 4.0783800e-04],
       [5.1559000e-05, 4.7187300e-04, 1.7553810e-03, 1.7694400e-04,
        6.4716500e-04, 3.4382680e-03]])
    MCoutSigmas=np.array([[
        4.21197797e-05, 2.31272372e-06, 1.74723324e-06, 4.25741706e-07,
        3.10346178e-06, 2.52063484e-06],
       [3.65129884e-05, 1.91705608e-06, 1.66811600e-06, 5.50653248e-07,
        2.80265927e-06, 2.93899217e-06],
       [6.16441814e-06, 1.11378124e-03, 3.81523646e-04, 2.90797290e-05,
        2.88574959e-04, 1.03886275e-04],
       [6.16337213e-06, 3.37265129e-04, 1.12718110e-03, 2.67607477e-05,
        7.97036610e-05, 8.83660876e-04],
       [6.23117164e-07, 2.47125365e-05, 2.30711155e-05, 4.03030100e-04,
        2.30198849e-05, 1.47022315e-05],
       [7.59573064e-06, 6.22798546e-05, 4.75219095e-05, 1.82665481e-05,
        2.75738979e-04, 4.33147175e-05],
       [7.11350961e-06, 4.06494670e-05, 4.52742105e-05, 2.31068142e-05,
        8.07919741e-05, 2.12341071e-04]])

    randMCout = np.random.normal(size=MCoutMeans.shape)*MCoutSigmas*width + MCoutMeans
    res = np.zeros(len(times))
    for i, (A0, tau) in enumerate(zip(A0s, taus)):
        res += A0*np.exp(-times/tau)*randMCout[det_id][i]
    return res
