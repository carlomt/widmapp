#!/usr/bin/env python
# coding: utf-8

from __future__ import division, print_function

import numpy as np

def readfile(fname):    
    output = []
    with open(fname) as f:
        next(f)
        for line in f:
            output.append(np.array(line.strip().split('\t')[1:],dtype='float64'))
    return output

