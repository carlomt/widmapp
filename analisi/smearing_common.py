#!/usr/bin/env python
# coding: utf-8

from __future__ import division, print_function

import sys
sys.path.append('./')
import os.path

import numpy as np
from common import num_representation, det_response, det_response_smear
from common import A0s, taus, MCout, times
#from common import activity, det_response_teo, det_contributions

widths = np.array([.05,.1,.15,.2,.25,.3,.35,.4], dtype=num_representation)
#widths = np.array([.05,.15,.4], dtype=num_representation)
widthsMatrix = np.array([0,.2,.4,.6,.8,1.,1.2,1.4], dtype=num_representation)


CPSall = np.empty(shape=(len(MCout),len(times)))
errCPSall = np.empty(shape=(len(MCout),len(times)))
for i in range(0,len(MCout)):
    resp = det_response_smear(times, A0s, taus, i, 0.2)
    CPSall[i] = resp
    errCPSall[i] = np.sqrt(resp)

errCPSall += num_representation(1.e-6) #to avoid zeros in the denominator of chi2

