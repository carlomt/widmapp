
# coding=utf-8
from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt

#define the function to fit CPS vs t
def Exp(t, *lista):
        sum=0
        #print(lista)
        #print(np.ndim(lista))
        k=len(lista)/2
        for i in range(k):
                sum +=lista[2*i]*np.exp(-t/lista[2*i+1])        
        #print(sum)
        return sum

#read files and convert from string to float without fist row and first column
MCout = []
with open('MC-out.txt') as f:
        next(f)
	for line in f:
		if line:
			MCout.append(line.strip().split('\t'))
for column in MCout:
    del column[0]
MCout = [map(float,i) for i in MCout]


A0 = [1294.0 ,0.,0.,0.,1457.2,0.] #initial activity in MBq
tau = [277.679,0.1,0.1,0.1,24.,0.1]  #radionuclide average life for different organs in hours
t = np.linspace (0.0,6.5 *tau[0], num=80.) #times
D = np.zeros((len(MCout),len(t)), dtype=np.float32) #will contain the signal on each detector at all times
err = np.zeros((len(MCout),len(t)), dtype=np.float32)
S0=np.zeros(len(A0)) #fit initial guess parameter
TauFit=np.zeros(len(A0)) #fit initial guess parameter
modelPredictions=np.zeros((len(MCout),len(t)), dtype=np.float32)
residuals=np.zeros((len(MCout),len(t)), dtype=np.float32)

for DetIndex in range(len(MCout)):
        for k in range(len(t)):                
                #sum on contributions to the signal from all organs
                for OrgID in range(len(A0)):
		        D[DetIndex][k] += MCout[DetIndex][OrgID]*A0[OrgID]*np.exp(-t[k]/tau[OrgID])
                        S0[OrgID]= MCout[DetIndex][OrgID]*A0[OrgID]
                        TauFit[OrgID]= tau[OrgID]
                err[DetIndex][k]= np.sqrt(D[DetIndex][k])
        
        lst = np.column_stack((S0, TauFit)).flatten()
        #print(lst)
        #Exp(t,*lst)

        #find optimal parameters  
        best_vals,cov= curve_fit(Exp,t,D[DetIndex][:], lst, err[DetIndex][:]/2., bounds=(0.,np.inf))
        print(" Optimal parameters (A00,tau0, A01, tau1, A02,tau2, S03, tau3, A04, tau4, A05, tau5):",best_vals, " Optimal covariance:", cov)
        modelPredictions[DetIndex]=Exp(t,*best_vals)
        residuals[DetIndex] = D[DetIndex][:]-modelPredictions[DetIndex][:]                

        #plot CPS vs t                                                                                                                                                                
        plt.errorbar(t,D[DetIndex][:],(err[DetIndex][:])/2.,None,'.', markersize=1, label='Det#%s'%DetIndex)
        plt.plot(t, modelPredictions[DetIndex],'red', linewidth= 0.5, label='BestFit#%s'%DetIndex)
        plt.legend()
        plt.title('CPS vs t')
        plt.xlabel('t[h]')
        plt.ylabel('CPS')
        #plt.savefig('CPSdet vs t.png')                                                                                                                                               
plt.grid(linestyle= '--')
plt.show()

#plot residuals
#for DetIndex in range(len(MCout)):
        #plt.scatter(modelPredictions[DetIndex], residuals[DetIndex], c='b',marker='.',linewidths='0.5')
        #plt.legend()
        #plt.title('Residuals vs modelPredictions Det#:%s'%DetIndex)
        #plt.xlabel('modelPredictions')
        #plt.ylabel('Residuals')
#plt.show()

