# 

## README
Monte Carlo simulation for the WIDMApp project.

This simulation is based on the Geant4 human_phantom example 

## Introduction

The human_phantom example models anthropomorphic phantoms for
Geant4 simulations.
Two models are available: MIRD [1] and ORNL [2] (Male and Female for each approach).

[1] W.S. Snyder, et al, "MIRD Pamphlet No. 5 Revised, Estimates of 
absorbed fractions for monoenergetic photon sources uniformly distributed 
in various organs of a heterogeneous phantom",
J. Nucl. Med. Suppl., no. 3, pp. 5-52, 1969.

[2] M. Cristy and K. F. Eckerman, "Specific absorbed fractions of energy 
at various ages from internal photon sources", ORNL/TM-8381/VI, Apr. 1987.

Note: Currently the ORNL phantom is under review.

We used only the MIRD phantom.

## Geometry 

The process of building a phantom is handled through the Builder 
design pattern.
The creation of coherent models of the human phantom is handled through 
an Abstract Factory design pattern.

The organs of the MIRD phantom are implemented in hard-code; the organs of 
the ORNL phantom are handled through GDML (Geometry Description Markup Language,
www.cern.ch/gdml). The materials of the MIRD model are defined in the
class G4HumanPhantomMaterial. The materials of the ORNL model are defined in 
the GDML files.
If using ORNL phantom model with no GDML set-up a segmentation
fault will be otained when running the simulation.

*Parameterized breast*
MIRD Female model: a breast is analytical and derives from the MIRD 
anthropomorphic phantom; the other breast derives from the model [3] and 
it is voxelised.

[3] D.R. Dance and R. A. Hunt, "Voxel breast phantom to represent breasts 
of different sizes and glandularities for use with a Monte Carlo simulation 
program", Report RMTPC 02/1005.

Tha main class for building the geometry is `G4HumanPhantomConstruction`. It has
as a data mamber an array of `G4VPhysicalVolume`, containing all the detectors.
Each detector can be built and positioned in the geometry using the `MyDetectorBuilder`
class. Once istantiated, this class has to be initialized calling its method
`SetMotherVolume` passing it as argument the pointer of the mother volume in
which you want to create the detectors. Each detector can be created with
the method `BuildDetector` which takes as arguments a `G4ThreeVector` to define
the centre of the detector and a `G4RotationMatrix` to define the orientation
of the detector in the mother volume. The detectors are cylidrical and for the
moment they have an hard-coded radius of 13 mm and  height 3 mm.

### Detectors
In the current configuration a certain number of detectors are positioned according to the elements added to fDetectorsOrigPos vector in HumanPhantomConstruction. Input to this vector should be the X and Z position. The inserted Y position will just be used to choose the side of the patient where to place the detector (up if y>0). The actual Y position (and angle) will be automatically calculated using the Trunk ellipse equation in order to have it almost tangent to the patient body (safeDistance=0.4cm is added).
There is also the possibility to simulate the mispositioning of the detectors. This is done through a variable (posMaxSmearing in G4HumanPhantomConstruction.hh) that is the max error in the radial direction from the original-ideal position of the detector (a random flat distribution of this width is used to move each detector independently). The value is set by default to 0, but can be changed via the '/detector/setMaxDetPosErr 3 cm' UI command (before run initialization!).
We exclude from this mispositioning det0-1, since we assume we are able to correctly place those on the thyroid.

## Physics

The Physics list used is the `G4EmStandardPhysics_option4` with the addition
of `G4DecayPhysics`.
The production threshold of secondary particles is set to 1 * mm.

## Primary particles

The G4 General Particle Source (GPS) is used to generate primary radiation field.
Macro primary.mac contains the definition of the primary radiation field.

### Macro file for generating primaries from a given organ
A python script has been added to create macros automatically. It will create a
macro file to simulate the radioactivity coming from a given organ, run it with the
name of the desidered organ (it has to match the name of one organ in the geometry).

createMacro.py script generates a GPS macro to generate primaries from a given organ.

Usage:
```
createMacro.py [-h] [-o OUTFILE] [-v] organname
```
where organname is a portion of the organ logical volume name, example:
```
python createMacro.py -o thyroid.mac thyroid
```
Possible command line arguments:
```
`-e [keV] energy of the primary photon
- g: adds to the .mac also the loading of geometry and beamOn command, so there is no more need for batch.mac macro.  In this way the organ.mac is enough to run the sim.
- n: number of primaries, necessary if -g option is selected
- nM: number of identical Macros to be genereated: used for detector movement simulations, together with:
- s: detector position smearing [cm, flat radial distance]
- R: directly submit the just created job to the farm

```

To create Iodine macros:
```
python createMacro.py -o lKidneyI.mac -g -n 10000000 LeftKidney
python createMacro.py -o rKidneyI.mac -g -n 10000000  RightKidney
python createMacro.py -o thyroidI.mac -g -n 10000000  Thyroid
python createMacro.py -o bladderI.mac -g -n 10000000  Bladder
python createMacro.py -o spleenI.mac -g -n 10000000  Spleen
python createMacro.py -o liverI.mac -g -n 10000000  Liver


````
To create Luttetium macros:
```
python createMacro.py -o lKidneyLuLow.mac -g -n 10000000 -e 113 LeftKidney
python createMacro.py -o rKidneyLuLow.mac -g -n 10000000 -e 113 RightKidney
python createMacro.py -o thyroidLuLow.mac -g -n 10000000 -e 113 Thyroid
python createMacro.py -o bladderLuLow.mac -g -n 10000000 -e 113 Bladder
python createMacro.py -o spleenLuLow.mac -g -n 10000000 -e 113 Spleen
python createMacro.py -o liverLuLow.mac -g -n 10000000 -e 113 Liver

python createMacro.py -o lKidneyLuHigh.mac -g -n 10000000 -e 208 LeftKidney
python createMacro.py -o rKidneyLuHigh.mac -g -n 10000000 -e 208 RightKidney
python createMacro.py -o thyroidLuHigh.mac -g -n 10000000 -e 208 Thyroid
python createMacro.py -o bladderLuHigh.mac -g -n 10000000 -e 208 Bladder
python createMacro.py -o spleenLuHigh.mac -g -n 10000000 -e 208 Spleen
python createMacro.py -o liverLuHigh.mac -g -n 10000000 -e 208 Liver


````

Detector smearing simulations:
```
python createMacro.py -o Bladder.mac -nM 10 -s 2 -g -R -n 100000000  Bladder
python createMacro.py -o Thyroid.mac -nM 10 -s 0 -g -R -n 100000000  Thyroid
python createMacro.py -o Liver.mac -nM 10 -s 0 -g -R -n 100000000  Liver
python createMacro.py -o rKidney.mac -nM 10 -s 0 -g -R -n 100000000  RightKidney
python createMacro.py -o lKidney.mac -nM 10 -s 0 -g -R -n 100000000  LeftKidney
python createMacro.py -o Spleen.mac -nM 10 -s 0 -g -R -n 100000000  Spleen

```

## To run batch similations on farm
```
bsub  ./phantom lKidneyI.mac 
bsub  ./phantom rKidneyI.mac 
bsub  ./phantom bladderI.mac 
bsub  ./phantom thyroidI.mac 
bsub  ./phantom spleenI.mac 
bsub  ./phantom liverI.mac 

bsub  ./phantom lKidneyLuLow.mac 
bsub  ./phantom rKidneyLuLow.mac 
bsub  ./phantom bladderLuLow.mac 
bsub  ./phantom thyroidLuLow.mac 
bsub  ./phantom spleenLuLow.mac 
bsub  ./phantom liverLuLow.mac 


bsub  ./phantom lKidneyLuHigh.mac 
bsub  ./phantom rKidneyLuHigh.mac 
bsub  ./phantom bladderLuHigh.mac 
bsub  ./phantom thyroidLuHigh.mac 
bsub  ./phantom spleenLuHigh.mac 
bsub  ./phantom liverLuHigh.mac 

```

## Energy deposit

~~The energy deposit is calculated in the organs of the phantom.
At the end of the execution of the simulation  the summary of the total energy deposit in 
each organ is print out.~~

Currently for ORNL model the energy deposition is calculated in the head only.

If the sensitivity is not set ( /bodypart/addBodyPart organName no), 
the energy deposit is not calculated in the specific organ.

The energy deposit is calculated in each voxel of the parameterised breast.

## Analysis
~~To activate the analysis, the application must be built with the option~~
~~WITH_ANALYSIS_USE=ON :~~
~~make ~~-DCMAKE_INSTALL_PREFIX=/path/to/geant4-installation~~ -DWITH_ANALYSIS_USE=ON /path/to/human_phantom/~~

The WITH_ANALYSIS_USE option is activated by default by the CMake file.
To switch it off:
```
make  -DWITH_ANALYSIS_USE=OFF
```

~~*SEQUENTIAL MODE*~~
~~output file: g4humanphantom.root containing~~
~~an ntuple with the Energy Deposit in each Body Part.~~
~~macro.C is provided to print the content of the ntuple in a ROOT~~
~~interactive analysis session.~~
~~ ~~
~~*MULTITHREAD MODE*~~
~~output files:~~
~~human_phantom.root_t0~~
~~..~~
~~..~~
~~human_phantom.root_t#~~
~~ ~~
~~where # is the number of threads~~
~~ ~~
~~type: source MergeFiles to merge the output of each thread in a single one~~
~~called human_phantom.root~~
~~macro.C is provided to print the content of the ntuple in a ROOT~~
~~interactive analysis session.~~

The output file name is set via the UI command:
```
/analysis/setFileName
```
the program merges the output and produce one single file also in multithreading mode

the analysis can be done with the plot.C class using ROOT:
```
root [0] .L plot.C
root [1] plot a("thyroid.root")
root [2] a.Loop()
```

There is a root macro to generate the MC output matrix:
```
root -l GetMatrix.C
```


## Macro files: example of different human phantoms

- default.mac is executed by default in the simulation, with visualisation
- batch.mac: macro to run in batch mode ( with no visualisation)
- adultMIRDFemale.mac: example to define a MIRD female human phantom
- adultMIRDMale.mac: example to define a MIRD male human phantom
- adultHead.mac: example how to define one piece of the anatomy
- adultORNLFemale.mac: example to define a ORNL female human phantom - THIS NEEDS GDML INSTALLED
- adultORNLMale.mac: example to define a ORNL male human phantom - THIS NEEDS GDML INSTALLED
- adultMIXFemale.mac : example of MIRD human female phantom with parameterised breast

## How to build the example 

If the user wants to run the example importing geometries via GDML, 
he/she needs to have built the persistency/gdml module by having
set the -DGEANT4_USE_GDML=ON flag during the CMAKE configuration step, 
as well as the -DXERCESC_ROOT_DIR=<path_to_xercesc> flag pointing to 
the path where the XercesC XML parser package is installed in your system.

- By default GDML is not configured to be used in the example. If the user wishes
to use GDML, he/she has to build the example with the following command:
cmake -DCMAKE_INSTALL_PREFIX=/path/to/geant4-installation -DWITH_GDML_USE=ON /path/to/human_phantom/

- if using GDML, the directory gdmlData must be copied in the directory where 
the simulation will be launched.

- Compile and link to generate the executable (in your CMAKE build directory):
 	      % make

- Execute the application:
 	      % ./phantom
	 
- Default macro: default.mac (MIRD, Female model) 

- Visualization macros: vrmlVis.mac, dawnVis.mac, openGLVis.mac

- Run simulation in batch mode: batch.mac
