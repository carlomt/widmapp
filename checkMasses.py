from __future__ import print_function, division
import subprocess
import re
import argparse
import sys
import math
import os
#from tabulate import tabulate
import numpy as np
import pandas as pd

parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", help="increase output verbosity",
					action="store_true")
parser.add_argument("-a", "--all", help="check all the organs",
					action="store_true")
parser.add_argument("-s", "--sel", help="selected col",
					action="store_true")
parser.add_argument("-l", "--latex", help="latex output",
					action="store_true")
   
args = parser.parse_args()

if args.verbose:
        print("Verbose output")

f = open("tmp.mac", "w")
f.write("/control/execute adultMIRDMale.mac \n")
f.write("/vis/drawTree \n")
f.close()

print("running Geant4 to find volumes")
MyOut = subprocess.Popen(['./phantom', 'tmp.mac'], 
						 stdout=subprocess.PIPE, 
						 stderr=subprocess.STDOUT)
stdout,stderr = MyOut.communicate()

decodedout = stdout.decode("utf-8")

OrganNames= ["Thyroid", "Kidney", "Bladder","Liver","Spleen"]

names = []
masses = []
volumes = []
sel = []

for iline, line in enumerate(decodedout.split('\n')):
    # if args.verbose:
    #     print(iline, line)
    trigger = False
    
    if re.match('^Volume of*', line):
        if args.verbose:
            print("Volume line:",line)
        lines = line.split()
        # if 
        if any(OrganName in lines[2] for OrganName in OrganNames) or (args.all and not "Detector" in lines[2]):
            names.append(lines[2])
            volumes.append(float(lines[4]))
            sel.append(any(OrganName in lines[2] for OrganName in OrganNames))
    if re.match('^Mass of*', line):
        if args.verbose:
            print("Mass line:",line)
        if any(OrganName in lines[2] for OrganName in OrganNames) or (args.all and not "Detector" in lines[2]):
            masses.append(float(line.split()[4]))

if args.verbose:
    print("end loop")            
    print("len{names):",len(names))
    print("len{masses):",len(masses))
    print("len{volumes):",len(volumes))
    print("len{sel):",len(sel))
            
# table = np.asarray([names, volumes, masses])
df = pd.DataFrame(data=[names,volumes,masses], index=["Name", "Volume","Mass"])
df = df.T
if args.sel:
    df['Selected']=sel

if args.latex:
    df = df.to_latex(index=False)
print(df)
# print(tabulate(table, headers=["Organ","Volume (cm^3)", "mass ()"]))
# for name, volume, mass in zip(names,volumes,masses):
#     print(name,volume,mass)

data = df.drop([5], axis=0)
data['MassPercentage'] = data['Mass']/data['Mass'].sum()
data['Activity'] =  data['MassPercentage'] * 2500*.7
data = data.append(df.loc[5,:])
data['Activity'][5] = 2500*.3
data['t_{1/2} bio'] = [1920., 12., 10., 6., 24., 17.]
print(data.to_latex())
