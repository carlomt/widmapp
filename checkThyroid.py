from __future__ import print_function, division
import subprocess
import re

f = open("tmp.mac", "w")
f.write("/control/execute adultMIRDMale.mac \n")
f.write("/vis/set/touchable  physicalWorld 0 physicalHead 0 physicalThyroid 0 \n")
f.write("/vis/touchable/dump \n")
f.close()

print("running Geant4")
MyOut = subprocess.Popen(['./phantom', 'tmp.mac'], 
                         stdout=subprocess.PIPE, 
                         stderr=subprocess.STDOUT)
stdout,stderr = MyOut.communicate()

decodedout = stdout.decode("utf-8")
# print("printing output")
# for iline, line in enumerate(decodedout.split('\n')):    
#     print(iline, line)

C = 1000
xmin = C
ymin = C
zmin = C
xmax = -C
ymax = -C
zmax = -C

for iline, line in enumerate(decodedout.split('\n')):    
    # xyz(1)=18.5 39.1 700
    if re.match('^xyz*', line):
    
        lines = line.split('=')
        # if len(lines)!=2: continue
        data = lines[1].split()
        data = [ float(x) for x in data ]        
        if data[0]<xmin: xmin=data[0]
        if data[0]>xmax: xmax=data[0]
        if data[1]<ymin: ymin=data[1]
        if data[1]>ymax: ymax=data[1]
        if data[2]<zmin: zmin=data[2]
        if data[2]>zmax: zmax=data[2]    

print(xmin,ymin,zmin)
print(xmax,ymax,zmax)
